FROM rocker/r-base:latest

RUN apt -y update && apt -y upgrade

RUN apt -y install curl

# Install quarto
RUN curl -LO https://github.com/quarto-dev/quarto-cli/releases/download/v1.3.302/quarto-1.3.302-linux-amd64.deb && \
    dpkg -i quarto-1.3.302-linux-amd64.deb && \
    apt install -f && \
    rm -f quarto-1.3.302-linux-amd64.deb && \
    quarto install tinytex
